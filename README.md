# reborn-mirrorlist

**NOTE 1:** mirror.arctic.lol (Finland) offline. Deleted:

```
# mirror.arctic.lol (Finland)
Server = http://mirror.arctic.lol/RebornMirror/

```

**NOTE 2:** Mirror **Clarkson** adedd. Now:

```
# RebornOS mirror.clarkson.edu
Server = https://mirror.clarkson.edu/RebornOS/RebornOS/
```

**NOTE 3:** 2022.01.30: New mirror added:

```
# OSDN RebornOS Repository 17
Server = https://ftp.sunet.se/mirror/osdn.net/storage/g/r/re/rebornos/repo/RebornOS/
```

reborn-mirrorlist to be used by others PKGBUILDs

reborn-mirrorlist raw:

```
https://gitlab.com/rebornos-team/rebornos-special-system-files/mirrors/reborn-mirrorlist/-/raw/master/reborn-mirrorlist
```

**NOTE 4:** (2022/06/15)

***Mirrors deleted***:

This mirror has not been updated since approximately May 21, 2022:
```
Server = https://mirrors.gigenet.com/OSDN/storage/g/r/re/rebornos/repo/RebornOS/
```
<br><br>

This mirror is empty. Only the folder exists:

```
Server = http://osdn.ip-connect.vn.ua/storage/g/r/re/rebornos/repo/RebornOS/
```
